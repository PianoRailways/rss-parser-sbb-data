/**
 * Source code for RSS feed parser discord bots
 * @author Sinsa#2674
 */
const {
  token,
    prefix,
  rssInfoFeed,
  rssConstructionFeed,
  rssIncidentFeed,
  rssDelayFeed,
    oevaInfoChannel,
    oevaConstructionChannel,
    oevaIncidentChannel,
    oevaDelayChannel,
  oevaServer,
  testServer,
  testserverSendingChannel,
} = require("./config.json");
const {version} = require("./package.json");
const Parser = require("rss-parser");
const Discord = require("discord.js");
const StripHtml = require("string-strip-html");
const fs = require("fs");
const parser = new Parser();
const client = new Discord.Client();
console.log("Listening on the following feeds:");
console.log(`Info-Feed: ${rssInfoFeed}`);
console.log(`Construction-Feed: ${rssConstructionFeed}`);
console.log(`Incident-Feed: ${rssIncidentFeed}`);
console.log(`Delay-Feed: ${rssDelayFeed}`);
let guilds;

client.once("ready", () => {
  guilds = client.guilds.cache;
  console.log(
    `Logged in as ${
      client.user.tag
    } at ${new Date()} ${new Date().toLocaleTimeString()}!`
  );
  leaveUnauthorizedServers();
  rssCheck(rssInfoFeed, "info", oevaInfoChannel);
  rssCheck(rssConstructionFeed, "construction", oevaConstructionChannel);
  rssCheck(rssIncidentFeed, "incident", oevaIncidentChannel);
  rssCheck(rssDelayFeed, "delay", oevaDelayChannel);
  setInterval(function () {
    rssCheck(rssInfoFeed, "info", oevaInfoChannel);
  }, 60000);
  setInterval(function () {
    rssCheck(rssConstructionFeed, "construction", oevaConstructionChannel);
  }, 60000);
  setInterval(function () {
    rssCheck(rssIncidentFeed, "incident", oevaIncidentChannel);
  }, 60000);
  setInterval(function () {
    rssCheck(rssDelayFeed, "delay");
  }, 60000);
  client.user.setPresence({ status: "online" });
  console.log("<init> Successfully started up!");
});

/**
 * Checks the RSS feed for changes that were made since the last check
 * @param feed the RSS Feed you'd like to check
 * @param name the name of the feed (will be used for the trackingfile creation)
 * @param channelId where a new post should be sent to
 * @since 1.0.0
 */
async function rssCheck(feed, name, channelId) {
  let lastPublishedArticle;
  try {
    lastPublishedArticle = JSON.parse(
      fs.readFileSync(`./last${name}FeedPost.json`)
    );
  } catch (error) {
    if (error.code === "ENOENT") {
      fs.writeFileSync(`./last${name}FeedPost.json`, "");
    }
    lastPublishedArticle = JSON.parse('{"items":[]}');
  }
  const article = await parser.parseURL(feed);
  //return if feed has no content
  if(article.items.length===0) {
    return;
  }
  if (
    JSON.stringify(lastPublishedArticle.items[0]) !==
    JSON.stringify(article.items[0])
  ) {
    // If the feed is the construction feed or the info feed, we do not wish to receive end notifications
    if((name === "construction" || name === "info") && article.items[0].title.toLowerCase().includes("endmeldung")) {
      return null;
    }
    const preparedMessage = await prepareMessage(article.items[0]);
    guilds.forEach((guild) => {
      const testserverChannel = guild.channels.resolve(
        testserverSendingChannel
      );
      const oevaChannel = guild.channels.resolve(channelId);
      if (testserverChannel) {
        testserverChannel.send(preparedMessage);
      }
      if (oevaChannel) {
        oevaChannel.send(preparedMessage);
      }
    });
    fs.writeFileSync(`./last${name}FeedPost.json`, JSON.stringify(article));
  }
}

client.on("guildCreate", (guild) => {
  if (oevaServer !== guild.id && testServer !== guild.id) {
    guild
      .leave()
      .then((result) =>
        console.info(`Left Guild ${result.name} because it was not allowed.`)
      );
  }
});

/**
 * Leaves Discord guilds that are not whitelisted (hardcoded whitelist)
 * @since 1.0.0
 */
function leaveUnauthorizedServers() {
  guilds.forEach((guild) => {
    if (oevaServer !== guild.id && testServer !== guild.id) {
      guild
        .leave()
        .then((result) =>
          console.info(`Left Guild ${result.name} because it was not allowed.`)
        );
    }
  });
}

/**
 * Prepare the message that will be sent into a discord channel
 * @param article the JSON object using the data that should be inserted
 * @returns a Promise containing a Discord RichEmbed Object
 * @since 1.0.0
 */
async function prepareMessage(article) {
  return new Discord.MessageEmbed()
    .setColor("RANDOM")
    .setTimestamp()
    .setURL(article.link)
    .setTitle(article.title)
    .setDescription(
      StripHtml(replaceALotOfBrShitQuickAndDirty(article.content.toString()))
        .result
    )
    .setThumbnail(client.user.avatarURL())
    .setFooter(article.pubDate + " / " + article.creator);
}

/**
 * This Function only exists because node is too shitty to recognize my regex aswell as accepting the replaceall Method. Don't blame me, blame node. This method is quick and dirty.
 * nodejs pls fix
 * @param string String you'd like to be unbr'd
 * @returns unbr'd string
 * @since 1.0.0
 */
function replaceALotOfBrShitQuickAndDirty(string) {
  return string
    .replace("<br>", "\n")
    .replace("<br/>", "\n")
    .replace("<br/>", "\n")
    .replace("<br/>", "\n")
    .replace("<br/>", "\n")
    .replace("<br/>", "\n")
    .replace("<br/>", "\n")
    .replace("<br/>", "\n")
    .replace("<br/>", "\n")
    .replace("<br/>", "\n")
    .replace("<br/>", "\n")
    .replace("<br/>", "\n")
    .replace("<br/>", "\n")
    .replace("<br/>", "\n")
    .replace("<br/>", "\n")
    .replace("<br/>", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n")
    .replace("<br />", "\n");
}

//region Command handling
/**
 * Handles all incoming commands
 * @since 2.1.0
 */
client.on('message', async message => {
  if(message.content.startsWith(prefix) && !message.author.bot) {
    const arguments = message.content.slice(prefix.length).split(/ +/);
    const cmd = arguments.shift().toLowerCase();
    switch(cmd) {
      case "version":
        message.channel.send(`Zurzeit befinde ich mich auf Version **${version}**`);
        break;
      case "eval":
        // we return here if the message author is not Sinsa
        if(message.author.id!=="392359476151451663") return;
        try
        {
          const code = arguments.join(' ');
          let evalthis = await eval(code);

          if (typeof evalthis !== 'string') evalthis = require('util').inspect(evalthis);
          if (typeof (evalthis) === 'string') evalthis = evalthis.replace(/`/g, '`' + String.fromCharCode(8203)).replace(/@/g, '@' + String.fromCharCode(8203));
          try {
            await message.channel.send(`Evaluated Successfully:\n\`\`\`${evalthis.substring(0, 1970)}\`\`\``);
          } catch (e1) {
            try {
              message.channel.send(`Evaluated Successfully:\n\`\`\`${evalthis}\`\`\``);
            } catch (e2) {
              message.channel.send('Evaluated Successfully:\n```Error in printing the Error. See the Console Log for more Details.```');
            }
          }
        }
        catch
            (e)
        {
          console.log(e);
          try {
            await message.channel.send(`An Exception was thrown:\n\`\`\`${e.substring(0, 1970)}\`\`\``);
          } catch (e1) {
            try {
              message.channel.send(`An Exception was thrown:\n\`\`\`${e}\`\`\``);
            } catch (e2) {
              message.channel.send('An Exception was thrown:\n```Error in printing the Error. See the Console Log for more Details.```');
            }
          }
        }
        break;
    }
  }
})
//endregion

client.login(token);
